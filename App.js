import React from "react";
import * as SplashScreen from "expo-splash-screen";
import { Asset } from "expo-asset";

import { StyleSheet, Image, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { useDispatch } from "react-redux";

import { initList } from "./src/stateManager/reducers/recipesSlice";
import { getRecipeList } from "./src/services/api.js";
import RecipesListScreen from "./src/components/RecipesEpic/RecipesListScreen";
import RecipeDetailsScreen from "./src/components/RecipesEpic/RecipeDetailsScreen";

import { ReactProvider } from "./src/stateManager/store";

const Stack = createStackNavigator();

const App = () => {
  const dispatch = useDispatch();
  getRecipeList().then((list) => dispatch(initList({ list })));
  return (
    <NavigationContainer style={AppStyles.container}>
      <Stack.Navigator>
        <Stack.Screen name="RecipesList" component={RecipesListScreen} />
        <Stack.Screen name="RecipeDetails" component={RecipeDetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default () => (
  <ReactProvider>
    <App />
  </ReactProvider>
);

const AppStyles = StyleSheet.create({
  container: {
    backgroundColor: "#F7F4EA",
    marginBottom: 20,
  },
  splash: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F7F4EA",
  },
});

/*

Palette de couleurs :
  Blanc cassé #F7F4EA
  Rose pale façon #DED9E2
  Bleu lavance #C0B9DD
  Petit mec bleu #80A1D4
  TURQUOISE MEDIUM #75C9C8
*/
