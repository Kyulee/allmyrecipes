import React from "react";
import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

import recipesSlice from "./reducers/recipesSlice";

const store = configureStore({
  reducer: {
    recipes: recipesSlice.reducer,
  },
});

export const ReactProvider = ({ children }) => (
  <Provider store={store}>{children}</Provider>
);
