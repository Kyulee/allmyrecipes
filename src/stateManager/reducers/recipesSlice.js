import { createSlice } from "@reduxjs/toolkit";

const recipesSlice = createSlice({
  name: "recipes",
  initialState: {
    list: [],
  },
  reducers: {
    initList: (state, action) => ({ ...state, list: action.payload.list }),
  },
});

export const { initList } = recipesSlice.actions;
export default recipesSlice;
