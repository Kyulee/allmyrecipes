import { API_SPOONACULAR_KEY, API_SPOONACULAR_URL } from "@env";
import axios from "axios";
import fakedata from "../../information_fake.json";

const recipesUrl = `${API_SPOONACULAR_URL}/recipes`;
const ingredientsUrl = `${API_SPOONACULAR_URL}/food/ingredients`;
const recipeListUrl = (query) =>
  `${recipesUrl}/complexSearch?apiKey=${API_SPOONACULAR_KEY}&query=${query}&number=5`;

const recipeByIdUrl = (id) =>
  `${recipesUrl}/${id}/information?apiKey=${API_SPOONACULAR_KEY}`;

export const getRecipeList = async (query) => {
  console.log(query);
  return (await axios(recipeListUrl(query))).data.results;
};

// export const getRecipeById = async (id) => fakedata;
export const getRecipeById = async (id) => {
  return (await axios(recipeByIdUrl(id))).data;
};
