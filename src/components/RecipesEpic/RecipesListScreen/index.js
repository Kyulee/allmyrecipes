import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

import { initList } from "./../../../stateManager/reducers/recipesSlice";
import { getRecipeList } from "../../../services/api";

const Item = ({ id, title, image }) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate("RecipeDetails", {
          id,
        });
      }}
    >
      <View style={styles.item}>
        <Image style={styles.tinyLogo} source={{ uri: image }} />
        <Text style={styles.title}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};
const RecipesListScreen = () => {
  const recipes = useSelector((state) => state.recipes.list);
  const dispatch = useDispatch();
  console.log(recipes);

  const [query, onChangeText] = useState("");
  const navigation = useNavigation();

  const renderItem = ({ item }) => {
    return <Item title={item.title} image={item.image} id={item.id} />;
  };

  useEffect(() => {
    getRecipeList(query).then((list) => dispatch(initList({ list })));
  }, [query]);

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchBar}
        onChangeText={(text) => onChangeText(text)}
        value={query}
      />
      {recipes.length > 0 ? (
        <FlatList
          data={recipes}
          renderItem={renderItem}
          keyExtractor={(item) => item.id.toString()}
        />
      ) : (
        <></>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F7F4EA",
  },
  searchBar: {
    borderRadius: 25,
    height: 40,
    margin: 10,
    padding: 5,
    borderColor: "grey",
    borderWidth: 1,
    backgroundColor: "#F7F4EA",
  },
  item: {
    borderRadius: 25,
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#75C9C8",
  },
  tinyLogo: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 15,
  },
  title: {
    fontSize: 18,
    maxWidth: 250,
    color: "#F7F4EA",
  },
});

export default RecipesListScreen;
