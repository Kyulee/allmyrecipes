import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, Image, ScrollView } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import { getRecipeById } from "../../../services/api";

const TextViewList = (e, stepId, label) => (
  <View key={`step${stepId}-${label}${e.id}`}>
    <Text>{e.name.toUpperCase()}</Text>
  </View>
);
const generateScrollview = (data, stepId) => {
  return (
    <View>
      <ScrollView>{data.map((item) => TextViewList(item, stepId))}</ScrollView>
    </View>
  );
};
const SeparationLine = () => <View style={styles.lineStyle} />;
const TitleIcon = ({ name, size, color }) => (
  <Ionicons
    name={name}
    size={size}
    color={color}
    iconStyle={{ marginRight: 150 }}
  />
);
const StepList = (step) => {
  const equipements =
    step.equipment && step.equipment.length > 0
      ? generateScrollview(step.equipment, step.number, "equipement")
      : null;
  return (
    <View key={`step${step.number}`} styles={styles.stepListSubSection}>
      <SeparationLine />
      <Text style={styles.titleSubSection}>Step {step.number}</Text>

      <View style={styles.flexRow}>
        <View>
          {equipements !== null && (
            <>
              <Text>Required equipements</Text>
              {equipements}
            </>
          )}
          <Text>{"\n"} </Text>
        </View>
      </View>
      <Text>{step.step}</Text>
    </View>
  );
};

const RecipesDetailsScreen = ({ route, navigation }) => {
  const { id } = route.params;
  const [recipeData, setRecipeData] = useState(undefined);

  useEffect(() => {
    if (!recipeData)
      getRecipeById(id).then((recipe) => {
        setRecipeData(recipe);
      });
  }, [id]);

  if (recipeData === undefined) {
    return (
      <View style={styles.container}>
        <Image
          style={styles.loading}
          source={require("../../../../assets/chef.gif")}
        ></Image>
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <Text style={styles.titleRecipe}>{recipeData.title.toUpperCase()} </Text>
      {recipeData && (
        <ScrollView style={styles.list}>
          <View style={styles.containerImage}>
            <Image
              key="imageRecipe"
              source={{ uri: recipeData.image }}
              style={styles.imageRecipe}
            />
          </View>
          <View style={styles.flexRow}>
            <Text>
              <TitleIcon name="md-people" size={24} color="black" />
              Servings : {recipeData.servings} persons
            </Text>
            <Text>
              <TitleIcon name="md-time" size={24} color="black" />
              Preparation Time : {recipeData.readyInMinutes} min
            </Text>
          </View>
          <SeparationLine />
          <Text style={styles.titleSection}>
            <TitleIcon name="md-basket" size={24} color="black" />
            Ingredients
          </Text>
          {recipeData.extendedIngredients.map((ing, index) => (
            <View
              key={`${ing.name.replace(/\s/g, "")}${index}`}
              style={styles.flexRow}
            >
              <View>
                <Text>
                  <Text style={styles.bulletPoint}>{"\u2022"}</Text>
                  {ing.name.toUpperCase()}
                </Text>
              </View>
              <View>
                <Text>
                  {ing.measures.metric.amount} {ing.measures.metric.unitShort}
                </Text>
              </View>
            </View>
          ))}
          <SeparationLine />
          <Text
            style={{
              fontSize: 16,
              textAlign: "center",
              justifyContent: "space-between",
            }}
          >
            <TitleIcon name="md-book" size={24} />
            Instructions
          </Text>
          {recipeData.analyzedInstructions.map((instruction) =>
            instruction.steps.map((s) => StepList(s))
          )}
        </ScrollView>
      )}
    </View>
  );
};
RecipesDetailsScreen.componentDidCatch = (error) => {
  console.error(error);
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  bulletPoint: { fontSize: 24 },
  list: {
    width: "100%",
    marginBottom: 15,
  },
  stepTitle: {
    fontSize: 11,
  },
  titleRecipe: {
    backgroundColor: "#75C9C8",
    fontSize: 25,
    width: "100%",
    color: "#F7F4EA",
    textAlign: "center",
  },
  containerImage: {
    justifyContent: "center",
    alignItems: "center",
  },
  imageRecipe: {
    height: 250,
    width: 400,
  },
  lineStyle: {
    borderWidth: 0.5,
    borderColor: "black",
    margin: 10,
  },
  titleSection: {
    fontSize: 16,
    textAlign: "center",
  },
  titleSubSection: {
    textAlign: "center",
  },
  flexRow: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  titleIcon: {
    marginRight: 12,
  },
  stepListSubSection: {
    marginBottom: 10,
  },
  loading: {
    width: 270,
    height: 200,
  },
});

export default RecipesDetailsScreen;
