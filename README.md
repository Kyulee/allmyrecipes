<center>
<img width=250 src="https://cdn.dribbble.com/users/2073082/screenshots/4352562/chef.gif " /> 
<h1>Mes recettes</h1>
</center>
Cette application a pour objectif de fournir des instructions pour des recettes diverses et variées.

<h2>Technologies utilisées</h2>

- Expo
- XCode
- React Native
- ReduxToolKit
- Insomnia

<h2>Liste des commandes de déploiement</h2>

- Installation

```
yarn install
```

- Lancement

```
expo start
```

<h2>Dependance en plus pour lancer le projet via XCode (seulement sur Mac)</h2>

[Doc d'installation XCode](https://docs.expo.io/workflow/ios-simulator/)

<h2>Diagramme de communication de l'API</h2>

```mermaid
    graph LR;
    API[API Spoonacular]-->|Get data|App[ReactNative App]
    App-->|Send Request| API;
    App-->Expo;
    Expo--> IOS;
    Expo--> Android;
    Expo--> Web;

```

<h2>Description de l'API</h2>

[API Spoonacular](https://spoonacular.com/food-api)

<h5>Routes utilisées</h5>

- [/complexSearch](https://spoonacular.com/food-api/docs#Search-Recipes-Complex)

```
@params :
    query:
        type: string
        description : La route a pour but de rechercher la liste de recettes.
        Si le parametre query est fourni, il permet de filtré cette liste par rapport à la valeur renseignée.
    apiKey:
        type: string
        description: Permet d'obtenir l'autorisation d'utilisation de l'API
```

- [/{id}/information](https://spoonacular.com/food-api/docs#Get-Recipe-Information)

```
@params :
    id:
        type: number
        description: Identifiant de la recette
    apiKey:
        type: string
        description: Permet d'obtenir l'autorisation d'utilisation de l'API
```
